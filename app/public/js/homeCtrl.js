'use strict';

angular.module('myApp.home', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/home', {
    templateUrl: 'public/views/home.html',
    controller: 'Home'
  });
}])

.controller('Home', ['$scope','$http', function($scope,$http) {

  var client = new Keen({
    projectId: "54175655248196382b0b045a",
    readKey: "df2e3c9c5c6e55d0b3e89a2c46e67e261e87d524eedacda878521ad736f9715bd208f90270a53d434246f1772f0f3a78527588586c377c0a245416ccedc65d16847151524479fd6dc90b9b43f2a5776c4a863508864818990152067a1e9d7aad00f0576415e7299b1b24c2525fc2e25e"
  });

  var weekStart = Date.today().previous().sunday().toLocaleDateString();
  var weekEnd = Date.today().next().saturday().toLocaleDateString();

//On Start Metrics//
  Keen.ready(function() {

  //Number of Users Creating Experiments//
    $scope.experimentUsers = new Keen.Query("count_unique", {
      eventCollection: "Experiments",
      targetProperty: "user",
      groupBy: "event_type",
      filters: [{"property_name":"event_type","operator":"eq","property_value":"New Experiment"}]
    });

  //Weekly New Experiment Creation//

    $scope.weeklyNewExperimentCreation = new Keen.Query("count", {
      eventCollection: "Experiments",
      timeframe: "this_week",
      interval: "daily",
      filters: [{"property_name":"event_type","operator":"eq","property_value":"New Experiment"}]
   });

    client.draw($scope.weeklyNewExperimentCreation, document.getElementById('new_experiment'), {
      chartType: 'columnchart',
      title: "New Experiments Created from " + weekStart + ' to ' + weekEnd,
      width: 1000,
      height: 250,
    });

  //Experiment Completion//
    $scope.totalNewExperiments = new Keen.Query("count", {
      eventCollection: "Experiments",
      timeframe: 'this_week',
      filters: [{"property_name":"event_type","operator":"eq","property_value":"New Experiment"}]
    });

    $scope.totalCompletedExperiments = new Keen.Query("count", {
      eventCollection: "Experiments",
      timeframe: 'this_week',
      filters: [{"property_name":"event_type","operator":"eq","property_value":"Completed Experiment"}]
    });

    client.run([$scope.totalNewExperiments,$scope.totalCompletedExperiments], function(res) {
      $scope.totalNew = res[0].result;
      $scope.totalComplete = res[1].result;
      $scope.completionRate = $scope.totalComplete*100/$scope.totalNew;

      window.chart = new Keen.Visualization({result: $scope.completionRate}, document.getElementById('experiment_completion'), {
        chartType: 'metric',
        title: 'Experiment Completion',
        width: 250,
        height: 250,
        chartOptions:{
          suffix:'%'
        }
      });
    });

  //Experiment Deletion//
    $scope.totalDeletedExperiments = new Keen.Query('count', {
      eventCollection: 'Experiments',
      timeframe: 'this_week',
      filters: [{'property_name':'event_type','operator':'eq','property_value':'Deleted Experiment'}]
    });

    client.draw($scope.totalDeletedExperiments, document.getElementById('deleted_experiments'), {
      chartType: 'metric',
      width: 250,
      height: 250,
      title:'Deleted Experiments'
    });


  //Post Location//
    $scope.postLocation = new Keen.Query("count", {
      eventCollection: "Posts",
      groupBy: "object_type",
      timeframe: 'this_week',
      filters: [{"property_name":"object_type","operator":"ne","property_value":"experimentStep"},{"property_name":"event_type","operator":"eq","property_value":"Post"}]
    });

    client.draw($scope.postLocation, document.getElementById('post_location'), {
      chartType: 'piechart',
      title: 'Post Locations',
      height: 250,
      width: 400
    });

  //Weekly Post Count//
    $scope.weeklyPosts = new Keen.Query('count', {
      eventCollection: 'Posts',
      timeframe: 'this_week',
      interval: 'daily',
      filters: [{"property_name":"object_type","operator":"ne","property_value":"experimentStep"}]
    });

    client.draw($scope.weeklyPosts, document.getElementById('weekly_posts'), {
      chartType: 'columnchart',
      title: 'Posts for ' + weekStart + ' to ' + weekEnd,
      height: 250,
      width: 1000
    });

  }) //End Keen Start


//Commenting Metrics//
  Keen.ready(function() {

    $scope.toDatePosts = new Keen.Query('count_unique', {
      eventCollection: 'Posts',
      groupBy: 'event_type',
      targetProperty: 'object_id',
      interval: 'daily',
      timeframe: {
        start: '2014-09-08T19:00Z',
        end: new Date()
      }
    });
  });

$scope.switchPostsView = function(view) {
  if(view === 'default') {
    client.run($scope.weeklyPosts, function(res) {
      window.chart = new Keen.Visualization(res, document.getElementById('weekly_posts'), {
        chartType: 'columnchart',
        title: 'Posts for ' + weekStart + ' to ' + weekEnd,
        height: 250,
        width: 1000
      });
    });
  }

  if(view === 'byLab') {
    $scope.weeklyPostsByLab = new Keen.Query('count', {
      eventCollection: 'Posts',
      timeframe: 'this_week',
      interval: 'daily',
      groupBy: 'lab'
    });

    client.run($scope.weeklyPostsByLab, function(res) {
      window.chart = new Keen.Visualization(res, document.getElementById('weekly_posts'), {
        chartType: 'columnchart',
        title: 'Posts for ' + weekStart + ' to ' + weekEnd,
        width: 1000,
        height: 250,
      });
    });
  }

};

$scope.switchNewExperimentView = function(view) {

  if(view === 'default') {
    client.run($scope.weeklyNewExperimentCreation, function(res) {
      window.chart = new Keen.Visualization(res, document.getElementById('new_experiment'), {
        chartType: 'columnchart',
        title: "New Experiments Created from " + weekStart + ' to ' + weekEnd,
        width: 1000,
        height: 250,
      });
    });
  }

  if(view === 'byLab') {
    $scope.weeklyNewExperimentCreationByLab = new Keen.Query("count", {
      eventCollection: "Experiments",
      timeframe: "this_week",
      interval: "daily",
      groupBy: 'lab',
      filters: [{"property_name":"event_type","operator":"eq","property_value":"New Experiment"}]
   });

    client.run($scope.weeklyNewExperimentCreationByLab, function(res) {
      window.chart = new Keen.Visualization(res, document.getElementById('new_experiment'), {
        chartType: 'columnchart',
        title: "New Experiments Created from " + weekStart + ' to ' + weekEnd,
        width: 1000,
        height: 250,
      });
    });
  }

  if(view === 'perUser') {
    client.run([$scope.weeklyNewExperimentCreation,$scope.experimentUsers], function(res) {
      $scope.averageWeeklyNewExperimentCreation = res[0];
      $scope.totalExperimentUsers = res[1].result[0].result;

      _.forEach($scope.averageWeeklyNewExperimentCreation.result, function(day) {
          day.value = Math.round(day.value*100/$scope.totalExperimentUsers)/100;
      });

      var today = Date.today();
      var start = Date.today().previous().sunday();

      window.chart = new Keen.Visualization($scope.averageWeeklyNewExperimentCreation, document.getElementById('new_experiment'), {
        chartType: 'columnchart',
        title: "New Experiments Created from " + weekStart + ' to ' + weekEnd,
        width: 1000,
        height: 250,
      });
    });
  }


};


}]);